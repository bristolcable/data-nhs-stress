# Stress-NHS
#### Cleaning and analysis of FOI Request data for stress-related absences in the NHS. For a series of articles & talks for the Bristol Cable newspaper.

Main article [Revealed: NHS staff in Bristol are being worked until they’re sick with stress](https://thebristolcable.org/2018/04/nhs-staff-workers-north-bristol-trust-are-being-worked-until-sick-with-stress/#graph)


Analysis done in Python using Pandas for data cleaning & transformation with Plotly for visualisation & exploration.

## Data sources
FOI Request data concerning stress related absences from four organisations
- North Bristol NHS Trust (NBT)
- University Hospitals Bristol HNS Foundation Trust (UHBT)
- South West Ambulance Service (SWAS)
- Avon and Wiltshire Mental Health Partnership NHS Trust (AWP) (not yet added to analysis)

Publically available data for FTE staff numbers from NBT, UHBT & SWAS (AWP not yet done) was manually collected from accounting reports.

## Data Questions
The questions I need to answer are below:

1. How have absence days due to stress/anxiety changed:  
    a. For all the services combined?  
    b. For each of the three services?  
2. How has the number of absence episodes due to stress/anxiety changed:  
    a. For NBT/SWAS together?  
    b. For NBT/SWAS separately?  
3. How has the average length of an anxiety episode changed (NBT & SWAS only)?
4. Which staff groups have the biggest rate of change (NBT & UHBT only)?  
5. What are the top 5 absence reasons (excluding Maternity & other non-sickness absences) for NBT & SWAS?   
6. What proportion of all sickness absences are due to stress & anxiety?



## Methodology Overview

1. Load, clean & transform data separately for each FOI request, combine FOI data with public data
2. Combine data sources together, applying naming conventions and aligning data to same level of granularity.
3. Explore combined data & output a summarised dataset for each interesting conclusion
4. Visualise & explain summarised datasets to support article


## Filing System

RawData contains all of the original data files; those concerning sickness & absences from the FOI request, as well as the FTE data collected from the annual reports.

CleanData contains the interim and final data files. The final data files used to answer the questions above are contained in the folder FinalDataForPublication. The interim data files consist of the cleaned data for each organisation, as well as the combined cleaned data.

Analysis contains all of the Jupyter Notebooks in which the data cleaning, transformation & analysis was done. These are numbered sequentially.

Viz contains a HTML version of the final Jupyter Notebook which answers the analytical questions. Previous versions of this analysis are in the Archive subfolder.



# Full Methodology

Note that is is also contained in the [HTML file](https://gitlab.com/bristolcable/data-nhs-stress/blob/master/Viz/Charts,%20copy,%20methods%20&%20caveats%20(includes%20correction%20to%20SWAS%20data).html) in the Viz folder.

## Introduction

The data used in this analysis is from 6 different sources. Several assumptions & exclusions had to be made in order to make the data suitable for analysis, as such, some inaccuracies will exist.

The following outlines the methods used to extract & clean the data and the assumptions & exclusions made when combining them.

The data comes from releases under the Freedom of Information Act and open data published by the following organisations: North Bristol Trust (NBT), University Hospitals Bristol Trust (UHBT) and South West Ambulance Service (SWAS). 


## FOI Data

Each of these trusts responded to an FOI request for absence data relating to stress & anxiety. Though the request was the same to each organisation, the level of detail and the format of the response differed for each trust:

###### North Bristol Trust
- Provided data from 2010-2017 in calendar years. 
- Provided data on the absence reason (including absences other than stress/anxiety) and the staff group. 
- Provided data on the number of episodes for each absence type, as well as the number of days (an episode can consist of multiple days).

###### University Hospitals Bristol Trust
- Provided month-level data from April 2011 to December 2017.
- Provided the number of absence days due to stress & anxiety split by staff group.

###### South West Ambulance Service
- Provided data which lists each absence between April 2013 & 6th January 2018. The start and end dates are shown for each absence, which allows us to calculate the number of days and the number of episodes.
- Information about the absence reason and department was also given.



## Publically Available Data


In order to account for the changing staffing levels, publically available information on the number of staff each trust employs was added to the analysis. This information is supplied as Full Time Equivalent (FTE) numbers. Further differences exist in this data:

FTE data is available for the NBT for calendar years 2010-2016 at a staff group level. Also for 2017 at Trust level.

FTE data is available for the UHBT for financial years 2010/11 - 2016/17 at a staff group level. Also until Feb 2018 at Trust level.

FTE data is available for the SWAS for financial years 2013/14 - 2016/17 at a staff group level (data available beforehand, but FOI is only from 2013). Also as of January 2018 at Trust-level.



### Data Differences
A table showing the data differences is available on the [Bristol Cable website](https://thebristolcable.org/wp-content/uploads/2018/04/nhs-stress-methodology.html).

In addition to the superficial differences, the main other data integrity problem was inconsistent reporting in staff group and department.

For UHBT, the staff groups reported on in the FOI request could not be accurately matched to the staff groups provided in the publicly available FTE data due to inconsistent terminology. 

For these cases, staff groups were only matched where there is a positive match.

For SWAS, the departments reported on in the FOI request did not correspond at all to the staff groups provided in the publicly available FTE data. Therefore, the SWAS data was not disaggregated to staff group. Instead only a Trust-level and Absence Reason-level data analysis was provided. 

The fundamental differences in the data can be summarised as follows:
- Reporting at both calendar year & financial year
- Difficulty mapping staff groups in UHBT FOI data to staff groups in SWAS accounts
- No accurate mapping for departments in SWAS FOI data to staff groups in SWAS accounts
- No absence reason (other than those for Stress & Anxiety) data for UHBT
- No episode data for UHBT


## Accounting and compensating for the differences


Because other absence data was not provided by UHBT, absences due to stress and anxiety as compared to other absence reasons was not reported for this trust, instead only being reported for NBT & SWAS.

In order to provide meaningful insight into the data, a compensation was made to adjust for the discrepancy in reporting for Financial & Calendar years: 
- For both UHBT & SWAS, the month-level FOI data was mapped directly to the financial years provided in the publically available FTE data. 
- These financial years were then 'converted' into calendar years, accepting that there would be a loss in accuracy.
    - For example, the Financial Year 2011/12 (covering the months from April 2011 to March 2012 inclusive) would be mapped to the 2011 calendar year. 
- This means that four months in each financial year would be misattributed to the corresponding calendar year. 

In order to give the most up-to-date picture of stress-related absences, the Trust-level FTE data is used in place of the staff group data where possible, despite not aligning perfectly.


Absences per FTE for UHBT & SWAS will be understated as there is a comparison of 9 months of absence data with staffing levels for a whole year. In order to provide the most up-to-date figures possible, it was necessary to adjust the FTE data for these trusts:

- UHBT supplied data until the end of December 2017. This means that for the 2017/18 financial year there are 3 months of absence data missing.

- SWAS supplied data until the 6th January 2018. This means that for the 2017/18 financial year there are ~3 months of absence data missing.

- The best option to retain data integrity whilst allowing for comparisons is to reduce the total number of FTE by one quarter in order to bring the FTE down to the same period of data as the absences were provided, though this is imperfect. 

- In this analysis, we're using FTE as a proxy for the number of days worked in a year, however FTE is actually the measure of staffing levels at a given point in time. By reducing FTE by 1/4, we are intentionally mis-stating the actual number of staff employed, but doing so in a way that allows us to make an  comparison with the provided sickness & absence data.

It must be noted that a mistake in this process led to the number of stress-related absences per FTE being overstated for SWAS in the April 2018 print edition of The Bristol Cable. These figures were rectified in the online copy and a correction will be published in the following edition.


## Other steps taken to clean & align the data

The staff groups did not directly match between and within data supplied by NBT & UHBT. This was easily fixed through text replacement. Stress & Anxiety absences were coded slightly differently. This was easily fixed through text replacement.

There were a significant quantity of Absence Reasons that were very closely aligned. These were recoded in order to reduce the number of categories. There were also slight differences in the way similar absence types were recorded by SWAS & NBT. These differences were manually aligned. All code used to align the absence reasons is available on [Gitlab](https://gitlab.com/bristolcable/data-nhs-stress).

The absence data for UHBT was censored if the number of absence in a month/staff group combination was below 5. This was done to protect individual staff members. There were 9 such cases where this happened. For these cases, the average of two days was imputed. This was calculated as  (1 + 2 + 3 + 4) / 4 = 2.5, rounded down to 2.

When calculating the top absence reasons we excluded maternity, leave and jury service and others, as we were purely looking at illness related absences. 

The full list of exclusions is available on [Gitlab](https://gitlab.com/bristolcable/data-nhs-stress).


## Quality Control

SWAS data was QC'd by creating a pivot table on the source data, then summing the number of days absent and counting the number of records. The absence rates per FTE were then calculated by hand for a few data points.

NBT Data was QC'd by checking the summed absence days after coding against the totals provided in the source data, then calculating the absence rates by hand for a few data points.

UHBT Data was QC'd against the raw data and several data points were checked.





